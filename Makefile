SHELL=/bin/bash
.DEFAULT_GOAL := build
MOSE_VERSION := $(shell git describe --abbrev=0 --tags)
SHA1_SHORT := $(shell git rev-parse --short HEAD)

.PHONY: clean
clean: project.clj
	lein clean

.PHONY: build
build: project.clj
	lein uberjar

.PHONY: run
run: project.clj
	lein run

.PHONY: docker-build
docker-build: Dockerfile
	docker build \
	    --tag=mock-service:$(MOSE_VERSION) \
		--tag=mock-service:$(SHA1_SHORT) \
		--tag=mock-service:latest .

.PHONY: docker-run
docker-run: Dockerfile
	docker logs --follow --timestamps $(shell docker run -p 3000:3000 -p 3001:3001 -d mock-service:$(SHA1_SHORT))

.PHONY: docker-stop
docker-stop: Dockerfile
	docker stop $(shell docker ps --filter='ancestor=mock-service' -q)
