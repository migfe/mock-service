# Mock Service (MOSE)

## Introduction

Mock Service is a Clojure REST application that is intended to be used to explore, understand, and test microservice 
architectures and Service Mesh solutions.

## Security

For the time being,  does not implement authentication.

## Usage

### Build and run Mock Service locally

By default, the application is bound to port 3000, and its health status to port 3001.

* `make clean` to clean build 
* `make build` to build the uberjar
* `make run` to run Mock Service locally
* or, use `java -jar target/uberjar/mock-service-0.0.0-standalone.jar` after `make build`
* `make docker-build` to build the docker image
* `make docker-run` to run Mock Service as a docker container
* `make docker-stop` to stop a running Mock Service container
* `curl localhost:3000` returns `Simon says, "Hello!"`
* `curl localhost:3000/request` returns the complete request
* `curl -v localhost:3001/liveness` returns status OK (200) if the application is healthy, 
    SERVICE UNAVAILABLE (503) otherwise.
* `curl -v localhost:3001/readiness` returns status OK (200) if the application is ready to accept traffic, 
    Service Unavailable (503) otherwise. 


### Commands

Commands are posted to Mock Service at /commands, the request body being the command specification as JSON.

The field `command` is always required, the fields `externalId` and `sender` are always optional. 
Others are required depending on the command, see examples below.

#### echo

Echo back the message in the command:

```json
{
  "externalId":1,
  "command":"echo",
  "message":"hello",
  "sender":"me"
}
```

```
curl -d '{"externalId":1,"command":"echo","message":"hello","sender":"me"}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

#### egress_http

Execute a GET HTTP request to another service:

```json
{
	"command": "egress_http",
	"message": "",
	"externalId": "1",
	"sender": "me",
	"parameters": {
		"uri": "httpbin.org/status/200"
	}
}
```

```
curl -d '{"command":"egress_http","message":"","externalId":"1","sender":"me","parameters":{"uri":"httpbin.org/status/200"}}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

#### egress_https

Execute a GET HTTPS request to another service:

```json
{
	"command": "egress_https",
	"message": "",
	"externalId": "1",
	"sender": "me",
	"parameters": {
		"uri": "httpbin.org/status/200"
	}
}
```

```
curl -d '{"command":"egress_https","message":"","externalId":"1","sender":"me","parameters":{"uri":"httpbin.org/status/200"}}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

#### liveness_up, liveness_down

Set liveness status to up or down, respectively. 

Note that the application still accepts requests (for the time being) even if the status is down.
The purpose of the liveness status is to guide the orchestrator, load balancer, service mesh etc.

```
curl -d '{"command":"liveness_up"}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

```
curl -d '{"command":"liveness_down"}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

Get the status using
```
curl -v localhost:3001/liveness
```

#### readiness_up, readiness_down

Set readiness status to up or down, respectively.

Note that the application still accepts requests (for the time being) even if the status is down.
The purpose of the readiness status is to guide the orchestrator, load balancer, service mesh etc.

```
curl -d '{"command":"readiness_up"}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

```
curl -d '{"command":"readiness_down"}' -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://localhost:3000/commands
```

Get the status using
```
curl -v localhost:3001/readiness
```

## Service

### JSON

How to convert JSON file to one liner that can be used as command line argument, or in an environment variable.
```
jq -c . file.json | sed 's/\"/\\\"/g'
```

## License

Copyright © 2020 Michael Gfeller

Distributed under the Eclipse Public License - v 2.0.
