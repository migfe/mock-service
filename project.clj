(defproject mock-service "0.0.0" ; this version never changes. use git tag to set version in final artifact
  :description "Simple Clojure REST application to explore Service Mesh implementations."
  :url "http://www.mgfeller.net"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v20.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.logging "1.1.0"]
                 [compojure "1.6.1"]
                 [cheshire "5.10.0"]
                 [http-kit "2.3.0"]
                 [tick "0.4.25-alpha"]                      ; https://juxt.pro/tick/docs/index.html - time handling
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-json "0.5.0"]                   ; https://github.com/ring-clojure/ring-json
                 [org.clojure/tools.cli "1.0.194"]          ; https://github.com/clojure/tools.cli - command line tools
                 [opentracing-clj "0.2.1"]
                 [io.jaegertracing/jaeger-client "1.3.1"]
                 [ch.qos.logback/logback-classic "1.2.3"]   ; as jaeger-client imports org.slf4j/slf4j-api but no impl
                 ]
  :main ^:skip-aot mockservice.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
