black:4000 -> white:3000 -> blue:3500

```
export JAEGER_SERVICE_NAME="black-service"
lein run -p 4000 -s 4001 -f resources/black-service-spec.json

export JAEGER_SERVICE_NAME="white-service"
lein run -f resources/white-service-spec.json

export JAEGER_SERVICE_NAME="blue-service"
lein run -p 3500 -s 3501 -f resources/blue-service-spec.json
```