(ns mockservice.core
  (:require [org.httpkit.server :as server]
            [org.httpkit.client :as http]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer :all]
            [clojure.pprint :as pp]
            [clojure.string :refer [join]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response :refer [response status]]
            [clojure.tools.logging :as log]
            [tick.alpha.api :as tick]
            [clojure.tools.cli :as cli]
            [cheshire.core :as cheshire]
            [opentracing-clj.ring :as tracing.ring]
            [opentracing-clj.core :as tracing]
            [opentracing-clj.propagation :as propagation]
            [clojure.string :as string])
  (:gen-class)
  (:import (io.opentracing.contrib.tracerresolver TracerResolver)))

(defonce status-ok 200)
(defonce status-service-unavailable 503)
(defonce default-service-port "3000")
(defonce default-status-port "3001")

(def app-status (atom {:liveness :up :readiness :up}))
(defn app-status-http [up_down] (if (= up_down :up) status-ok status-service-unavailable))
(defn application-status [probe] (status (response {:probe probe}) (app-status-http (probe @app-status))))
(defn liveness-status [req] (application-status :liveness))
(defn readiness-status [req] (application-status :readiness))

(defn simon-says [req]
  {:status  status-ok
   :headers {"Content-Type" "text/plain"}
   :body    "Simon says, \"Hello!\""})

(defn echo-request [req]
  {:status  status-ok
   :headers {"Content-Type" "application/json"}
   :body    (with-out-str (pp/pprint req))})

(defn uuid [] (.toString (java.util.UUID/randomUUID)))

(defn egress [command-input protocol]
  (let [uri (get-in command-input ["parameters" "uri"] "httpbin.org/status/200")
        prot (if (= protocol :http) "http" "https")
        {:keys [status headers body error] :as resp} @(http/get (join [prot "://" uri]) {:as :text})]
    resp))

(defmulti execute-command (fn [command-input] (command-input "command")))
(defmethod execute-command :default [command-input] {:error-message "unknown command"})
(defmethod execute-command "echo" [command-input] (command-input "message"))
(defmethod execute-command "egress_http" [command-input] (egress command-input :http))
(defmethod execute-command "egress_https" [command-input] (egress command-input :https))
(defmethod execute-command "liveness_up" [command-input] (swap! app-status assoc :liveness :up))
(defmethod execute-command "liveness_down" [command-input] (swap! app-status assoc :liveness :down))
(defmethod execute-command "readiness_up" [command-input] (swap! app-status assoc :readiness :up))
(defmethod execute-command "readiness_down" [command-input] (swap! app-status assoc :readiness :down))

(defn command-handler [req]
  (log/info "handling command: " req)
  (let [started (tick/now)
        commandId (uuid)
        command-input (:body req)
        command (command-input "command")
        command-output ((get-method execute-command command) command-input)]
    (let [completed (tick/now)]
      (status (response {:commandOutput      command-output
                         :status             "COMPLETED"
                         :commandCreatedAt   (str started)
                         :commandCompletedAt (str completed)
                         :commandDuration    (str (tick/between completed started))
                         :commandInput       command-input})
              status-ok))))

(defonce default-service-spec
         {:instance-id  "" ;; can be used to identify a specific instance of a service, can be handy in testing and exploring.
          :name         "default",
          :delay-before 10,
          :delay-after  45,
          :upstream     [{:name "red", :delay-before 75, :delay-after 50, :url "http://httpbin.org/status/200"}
                         {:name "green", :delay-before 25, :delay-after 50, :url "http://httpbin.org/headers"}]}
         )

(def service-spec (atom default-service-spec))

(defn call-upstream [upstream-spec]
  (log/info (str "calling upstream service '" (:name upstream-spec) "' at '" (:url upstream-spec) "'"))
  (tracing/with-span [span {:name (str (:name upstream-spec) " -> " (:url upstream-spec))}]
                     (let [started (tick/now)
                           tracing-headers (propagation/inject :http)
                           _ (Thread/sleep (:delay-before upstream-spec))
                           {:keys [status headers body error]} @(http/get (:url upstream-spec)
                                                                          {:as :text :headers tracing-headers})
                           _ (Thread/sleep (:delay-after upstream-spec))
                           completed (tick/now)]
                       {:name        (:name upstream-spec)
                        :startedAt   (str started)
                        :completedAt (str completed)
                        :duration    (str (tick/between completed started))
                        :response    {:headers headers
                                      :status  status
                                      :body    (try (cheshire/parse-string body true) (catch Exception e body))
                                      :error   error
                                      }
                        })))

(defn call-upstream-services [service-name upstream-services]
  (log/info (str "calling upstreams services of " service-name " " upstream-services))
  (doall (map call-upstream upstream-services)))

(defn service-handler [req]
  (log/info (str "executing service '" (:name @service-spec) "', id='" (:id @service-spec)) "'")
  (let [started (tick/now)
        _ (Thread/sleep (:delay-before @service-spec))
        result (call-upstream-services (:name @service-spec) (:upstream @service-spec))
        completed (tick/now)
        _ (Thread/sleep (:delay-after @service-spec))]
    (status (response {:id              (:id @service-spec)
                       :name            (:name @service-spec)
                       :startedAt       (str started)
                       :completedAt     (str completed)
                       :duration        (str (tick/between completed started))
                       :upstream-result result
                       })
            status-ok)))

(defroutes app-routes
           (GET "/" [] simon-says)
           (GET "/request" [] echo-request)
           (POST "/commands" [] (wrap-json-body (wrap-json-response command-handler)))
           (GET "/service" [] (wrap-json-response service-handler))
           (route/not-found "Error, page not found!"))

(defroutes state-routes
           (GET "/liveness" [] (wrap-json-response liveness-status))
           (GET "/readiness" [] (wrap-json-response readiness-status))
           (route/not-found "Error, page not found!"))

(def cli-options-spec
  [
   ["-p" "--port PORT" "Service port number"
    :parse-fn #(Integer/parseInt %)                         ;; #() declares an anonymous function
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-s" "--status-port PORT" "Status port number"
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-f" "--file FILE" "Service specification as JSON (encoded in utf-8)"]
   ["-i" "--instance-id ID" "id of a specific service instance, useful for testing and exploring"]
   ])

(defn tracing-operation-name
  [ring-request]
  (str (:name @service-spec) " " (string/upper-case (:request-method ring-request)) " " (:uri ring-request)))

(defn -main
  "This is the main entry point"
  [& args]
  (let [cli-options (cli/parse-opts args cli-options-spec)

        service-port (or (get-in cli-options [:options :port])
                         (Integer/parseInt (or (System/getenv "MOSE_SERVICE_PORT")
                                               default-service-port)))
        status-port (or (get-in cli-options [:options :status-port])
                        (Integer/parseInt (or (System/getenv "MOSE_STATUS_PORT")
                                              default-status-port)))
        service-instance-id (or (get-in cli-options [:options :instance-id])
                                (System/getenv "MOSE_INSTANCE_ID"))
        service-spec-file (if-let [opt-var (get-in cli-options [:options :file])]
                            (cheshire/parse-string
                              (slurp opt-var :encoding "UTF-8") true))
        service-spec-env (if-let [env-var (System/getenv "MOSE_SERVICE_SPEC")]
                           (cheshire/parse-string env-var true))
        service-spec-init (or service-spec-file (or service-spec-env default-service-spec))]

    ;; https://github.com/jaegertracing/jaeger-client-java/blob/master/jaeger-core/README.md
    ;; Need to set environment variable JAEGER_SERVICE_NAME for this to work.
    ;; Services can have the same name, but can also have different names. This is reflected in the Jaeger UI.
    (if-let [envvar (System/getenv "JAEGER_SERVICE_NAME")]
      (do
        (log/info (str "Environment variable 'JAEGER_SERVICE_NAME': '" envvar "'"))
        (alter-var-root #'tracing/*tracer* (constantly (TracerResolver/resolveTracer))))
      (log/info "Environment variable 'JAEGER_SERVICE_NAME' is not defined"))

    (reset! service-spec service-spec-init)

    (if service-instance-id (swap! service-spec assoc :instance-id service-instance-id))

    (server/run-server (tracing.ring/wrap-opentracing
                         (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false))
                         tracing-operation-name)
                       {:port service-port})
    (server/run-server (wrap-defaults state-routes
                                      (assoc-in site-defaults [:security :anti-forgery] false))
                       {:port status-port})

    (log/info "cli-options:" cli-options)
    (log/info "service spec:" @service-spec)
    (log/info (str "starting service server at http:/127.0.0.1:" service-port "/"))
    (log/info (str "starting status server at http:/127.0.0.1:" status-port "/"))))